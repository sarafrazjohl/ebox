#!/usr/bin/env bash

#Let ubuntu know it should expect a non-interactive shell (Untested)
export DEBIAN_FRONTEND=noninteractive

# Varaibles (for git and other apps)
name=Johl
email=johl@sarafraz.com
hostName=ebox
password=eB0x

# Set Host Name
echo "MILESTONE - HostName"
oldHostName=`cat /etc/hostname`
hostname $hostName
sed -i "s/$oldHostName/$hostName/g" /etc/hostname
sed -i "s/$oldHostName/$hostName/g" /etc/hosts

# Set Timezone (Melbourne)
echo "MILESTONE - Timezone"
mv -f /etc/localtime /etc/localtime.bak
ln -sf /usr/share/zoneinfo/Australia/Melbourne /etc/localtime

# Update Linux
echo "MILESTONE - OS Update"
apt-get update

# General tools
echo "MILESTONE - Install General Tools"
apt-get install -y zip
apt-get install -y ntp
apt-get install -y curl
# apt-get install -y sendmail
# apt-get install -y buildessential

# Add 3rd party repos
# echo "MILESTONE - Add 3rd party repos"
apt-get -y install python-software-properties
add-apt-repository -y ppa:ondrej/php
apt-get update

# Apache: Download & Install
echo "MILESTONE - Apache2"
apt-get install -y apache2

# Apache: Set hostname
echo "ServerName localhost" >> /etc/apache2/httpd.conf

# Apache: Make www accessible from host
echo "MILESTONE - Make www accessible from host"
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant/www /var/www
fi

# Apache: Enable modules & Restart
echo "MILESTONE - Aliases"
a2enmod alias
a2enmod mime
a2enmod rewrite
service apache2 restart

# PHP: Download & Install
echo "MILESTONE - php"
apt-get -y install php7.0

# PHP: Install extensions and restart apache
echo "MILESTONE - php modules"
apt-get install -y php7.0-mysql php7.0-curl php7.0-gd php7.0-bcmath php7.0-mbstring php7.0-mcrypt php7.0-xml libapache2-mod-php7.0

# PHP: Config
echo "MILESTONE - Config"
sed -i "s/memory_limit =/memory_limit = 256M;/g" /etc/php/7.0/apache2/php.ini
sed -i "s/post_max_size =/post_max_size = 64M;/g" /etc/php/7.0/apache2/php.ini
sed -i "s/upload_max_filesize =/upload_max_filesize = 64M;/g" /etc/php/7.0/apache2/php.ini
sed -i "s/error_reporting =/error_reporting = E_ALL | E_STRICT;/g" /etc/php/7.0/apache2/php.ini
sed -i "s/display_errors =/display_errors = On;/g" /etc/php/7.0/apache2/php.ini
sed -i "s/html_errors =/html_errors = On;/g" /etc/php/7.0/apache2/php.ini
sed -i "s/mysql.connect_timeout =/mysql.connect_timeout = 10;/g" /etc/php/7.0/apache2/php.ini
sed -i "s/expose_php =/expose_php = Off;/g" /etc/php/7.0/apache2/php.ini
service apache2 restart

# MySql: Set configuration
echo "MILESTONE - mysql-debconf"
debconf-set-selections <<< "mysql-server mysql-server/root_password password $password"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $password"

# MySql: Download & Install
echo "MILESTONE - mysql-server"
apt-get install -y mysql-server

# MySql: Changer Parameters
echo "MILESTONE - mysql-parameters"
sed -i "s/bind-address/bind-address\t\t= 0.0.0.0\n#bind-address/g" /etc/mysql/my.cnf

# Install Ruby
echo "MILESTONE - Ruby"
apt-get install -y ruby-full

# Install Node
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
apt-get install -y nodejs

# Install Git
echo "MILESTONE - Git"
apt-get install -y git
git config --global user.name "$name"
git config --global user.email "$email"

# Install Composer
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Adminer
echo "MILESTONE - Adminer"
rm -f /var/www/adminer.php
wget "http://www.adminer.org/latest.php" -O /var/www/adminer.php

# Webmin: Download & Install
echo "MILESTONE - Webmin Install"
sh -c 'echo "deb http://download.webmin.com/download/repository sarge contrib" > /etc/apt/sources.list.d/webmin.list'
wget -qO - http://www.webmin.com/jcameron-key.asc | apt-key add -
apt-get update
apt-get install -y webmin

# Webmin: Setup & Restart
echo "MILESTONE - Webmin Config"
sed -i "s/port=10000/port=8080/" /etc/webmin/miniserv.conf
sed -i "s/listen=10000/listen=8080/" /etc/webmin/miniserv.conf
sed -i "s/ssl=1/ssl=0/" /etc/webmin/miniserv.conf
/etc/init.d/webmin restart

# Set root password
echo "MILESTONE - Set Root Password"
echo -e "$password\n$password\n" | passwd root

# Set prompt
echo -n "export PS1='" >> /home/vagrant/.bashrc
echo -n '\n\[\e[1;37m\]\[\e[1;32m\]\u\[\e[0;39m\]@\[\e[1;36m\]\h\[\e[0;39m\]:\[\e[1;33m\]\w\[\e[0;39m\]\[\e[1;35m\]$(__git_ps1 " (%s)")\[\e[0;39m\] \[\e[1;37m\]\[\e[0;39m\]\n$ ' >> /home/vagrant/.bashrc
echo "'" >> /home/vagrant/.bashrc
