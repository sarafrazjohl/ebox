#!/bin/bash

### Input
action=$1
domain=$2
domainRoot=$3
documentRoot=$3

### Sanitize Action
if [ "$action" != 'create' ] && [ "$action" != 'enable' ] && [ "$action" != 'disable' ] && [ "$action" != 'soft-delete' ] && [ "$action" != 'hard-delete' ]
	then
		echo $"Example usage: $ sudo vh action domain [document_root]"
		echo $"Following actions are available:"
		echo -e $" - create \t Creates a new host, config & data structure"
		echo -e $" - enable \t Links from sites-enabled"
		echo -e $" - disable \t Unlinks from sites-enabled"
		echo -e $" - soft-delete \t Deletes Config"
		echo -e $" - hard-delete \t Deletes Config & Data"
		echo $""
		echo -e $"Create Action: Document root defaults to: $wwwDir/domain/public_html"
		exit 1;
fi

### Sanitize Domain
while [ "$domain" == "" ]
do
	echo -e $"Please provide domain. ex. johl.local"
	read domain
done

### Constants
wwwDir='/var/www/'
sitesEnabled='/etc/apache2/sites-enabled/'
sitesAvailable='/etc/apache2/sites-available/'

### Variables
owner=$(who am i | awk '{print $1}')
sitesEnabledFile=$sitesEnabled$domain.conf
sitesAvailableFile=$sitesAvailable$domain.conf

### Check SUDO
if [ "$(whoami)" != 'root' ]; then
	echo $"You have no permission to run $0 as non-root user. Use sudo"
		exit 1;
fi

### If root dir is empty, build it
if [ "$domainRoot" == "" ]; then
	domainRoot="$wwwDir$domain"
	documentRoot="$wwwDir$domain/public_html"
fi

### if domainRoot starts with '/', don't use /var/www as default starting point
if [[ "$domainRoot" =~ ^/ ]]; then
	echo -n ""
else
	domainRoot=$wwwDir$domainRoot
	documentRoot=$wwwDir$documentRoot
fi

### Process Action:Create
if [ "$action" == 'create' ]
	then
		### Ensure directory exists
		if ! [ -d $documentRoot ]; then
			mkdir -p $documentRoot
			chmod -r 755 $domainRoot
		fi

		### Set/Reset owner
		if [ "$owner" == "" ]; then
			chown -R $(whoami):$(whoami) $domainRoot
		else
			chown -R $owner:$owner $domainRoot
		fi

		### Check if domain config already exists
		if [ -e $sitesAvailableFile ]; then
			echo -e $"Error: This virtual host is already configured."
		else
			if ! echo	"<VirtualHost *:80>
							ServerName $domain
							DocumentRoot "$documentRoot"
							<Directory "$documentRoot">
								Require all granted
						        Options FollowSymLinks
						        AllowOverride all
							</Directory>
							ErrorLog $domainRoot/error.log
						</VirtualHost>" > $sitesAvailableFile
			then
				echo -e $"Error: Unable to create $sitesAvailableFile file"
				exit 1;
			else
				echo -e $"Success: Created virtual host $domain at $domainRoot"
			fi
		fi

		### Enable virtual host
		ln -s "$sitesAvailableFile" "$sitesEnabledFile"

		### Restart Apache
		service apache2 restart
fi

### Process Action:Enable
if [ "$action" == 'enable' ]
	then
		### Create link
		if [ -e $sitesAvailableFile ]; then
			rm -f "$sitesEnabledFile"
			ln -s "$sitesAvailableFile" "$sitesEnabledFile"
		else
			echo -e $"Error: Unable to find Config file"
		fi

		### Restart Apache
		service apache2 restart
fi

### Process Action:Disable
if [ "$action" == 'disable' ]
	then
		### Remove link
		rm -f "$sitesEnabledFile"

		### Restart Apache
		service apache2 restart
fi

### Process Action:Delete
if [ "$action" == 'soft-delete' ]
	then
		### Remove config & link
		rm -f $sitesAvailableFile
		rm -f "$sitesEnabledFile"

		### Restart Apache
		service apache2 restart
fi

### Process Action:Clean
if [ "$action" == 'hard-delete' ]
	then
		### Stop Apache
		service apache2 stop

		### Remove config & link
		rm -f $sitesAvailableFile
		rm -f "$sitesEnabledFile"
		echo -e $"Deleted host config and link"

		### Ask if directory should be deleted too
		rm -rf "$domainRoot"
		echo -e $"Deleted host directory"

		### Start Apache
		service apache2 start
fi

exit 0;