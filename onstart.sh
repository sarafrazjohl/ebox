#!/usr/bin/env bash

#Let ubuntu know it should expect a non-interactive shell (Untested)
export DEBIAN_FRONTEND=noninteractive

# Copy scripts into box
cp /vagrant/scripts/vh.sh /usr/local/sbin/vh
chmod +x /usr/local/sbin/vh
sed -i -e 's/\r$//' /usr/local/sbin/vh

#Start Apache server
echo "MILESTONE - Starting Apache"
service apache2 start