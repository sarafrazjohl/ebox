# Easy setup

### Install the box behind proxy
1) Download the [box file](https://atlas.hashicorp.com/ubuntu/boxes/trusty64/versions/20160826.0.1/providers/virtualbox.box) and save as **mybox.box** in any directory.


2) Run the following command in that directory:

    vagrant box add Ubuntu/Trusty64 mybox.box

### Customise your username, password
* Modify top few lines in **onsetup.sh**

## Firing it up
Using terminal, go to directory where this repository has been cloned and run

    vagrant up

## Login
[http://192.168.60.111/adminer.php] (http://192.168.60.111/adminer.php)

>*Default username is root and password is eB0x*

## Create a new vhost
    vagrant ssh
    sudo vh create *custom.dev* /src/*custom.dev*/public
